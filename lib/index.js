const pathOr = require('ramda/src/pathOr');
 
class CorePlugin {
    constructor() {
        this.context = null;
        this.config = {
            param: '<default value>'
        };
        this.hooks = {};
        this.pipes = {
            'document:beforeCreate': 'checkBeforeCreateConditions',
            'document:afterCreate': 'checkAfterCreateConditions',
        };
        this.controllers = {
            trackingController: {
                trackEvent: 'trackEvent',
            },
        };
        this.routes = [
            {
                verb: 'post',
                url: '/trackEvent',
                controller: 'trackingController',
                action: 'trackEvent'
            },
        ];
        this.accounts = {}
    }
 
    init(customConfig, context) {
        this.config = Object.assign(this.config, customConfig);
        this.context = context;
    }
 
    async checkBeforeCreateConditions(request, callback) {
        // console.log('ACTION: checkBeforeCreateConditions', JSON.stringify(request, null, 2));
        if (
            request.input.controller === 'document' &&
            request.input.action === 'create' &&
            (request.input.resource.index === 'system' || request.input.resource.index === 'test-system') &&
            request.input.resource.collection === 'events'
        ) {
            const accountIdentifier = `${request.input.resource.index}-${request.input.body.accountId}`
            if (!request.input.body.accountId) {
                console.log(`Missing account id ${request.input.body.accountId}`);
                callback(new this.context.errors.PreconditionError(`Missing account id ${request.input.body.accountId}`), null);
            }
 
            if (!this.accounts[accountIdentifier]) {
                const account = await this.context.accessors.sdk.document.get(
                    request.input.resource.index,
                    'accounts',
                    request.input.body.accountId
                )
                if (!account) {
                    console.log(`Provided account id ${request.input.body.accountId} is not valid`);
                    callback(new this.context.errors.NotFoundError(`Provided account id ${request.input.body.accountId} is not valid`), null);
                } else {
                    this.accounts[accountIdentifier] = account;
                }
            }
 
            request.input.body.isActive = pathOr(false, ['_source', 'isActive'], this.accounts[accountIdentifier])
            request.input.body.selector = `${request.input.body.accountId}${request.input.body.isActive}`
            callback(null, request);
        } else {
            callback(null, request);
        }
    }
 
    async checkAfterCreateConditions(request, callback) {
        // console.log('ACTION: checkAfterCreateConditions', JSON.stringify(request, null, 2));
        if (
            request.input.controller === 'document' &&
            request.input.action === 'create' &&
            (request.input.resource.index === 'system' || request.input.resource.index === 'test-system') &&
            request.input.resource.collection === 'accounts'
        ) {
            const accountIdentifier = `${request.input.resource.index}-${request.input.body.accountId}`
            if (this.accounts[accountIdentifier]) {
                console.log("Updating isActive information of locally stored account id")
                this.accounts[accountIdentifier].isActive = request.input.body.isActive
            }
        }
        callback(null, request);
    }
 
    async trackEvent(request) {
        // console.log('Received incoming event', request.input.body)
        const event = await this.context.accessors.sdk.document.create(
            request.input.resource.index,
            'events',
            request.input.body
        );
        const json = JSON.stringify({ event });
        return json;
    }
}
 
module.exports = CorePlugin;
 


