 Feature: Tracking controller

   Scenario: Login with username and password
    Given I'm logged in Kuzzle as user "kuzzleadmin" with password "55e4Cfc993951039fc7144a45e837b3432d45g993e1233"

   Scenario: Testing database document count
    When tests are initialized test indexes "test-system" with collections "accounts" and "events" are created
    Then I should receive exists object with index, accounts and events properties set to "true"
    When tests are initialized count all documents
    Then I should receive "0" documents in events collection and "2" documents in accounts collection

   Scenario: Testing selector and isActive properties after event creation
    When I call the plugin route "test-system":"events" with args:
      | body.accountId | slovenia |
      | body.data   | test data  |
      | body.position.lat | 46.0569  |
      | body.position.lon | 14.5057  |
    Then I should receive additional property selector with content: "sloveniatrue" and property isActive with content: "true"
    When I call the plugin route "test-system":"events" with args:
      | body.accountId | spain |
      | body.data   | test data  |
      | body.position.lat | 40.2085  |
      | body.position.lon | -3.7310  |
    Then I should receive additional property selector with content: "spainfalse" and property isActive with content: "false"

   Scenario: Testing required parameters for event creation
    When I call the plugin route "test-system":"events" with args:
      | body.data   | test data  |
      | body.position.lat | 46.0569  |
      | body.position.lon | 14.5057  |
    Then I should receive response with precondition error with http status code: "412"
    When I call the plugin route "test-system":"events" with args:
      | body.accountId | france |
      | body.data   | test data  |
      | body.position.lat | 46.2276  |
      | body.position.lon | 2.2137  |
    Then I should receive response with precondition error with http status code: "404"

   Scenario: Testing pub/sub system
    When I subscribe to "slovenia" channel for route "test-system":"events" and trigger event with args:
      | body.accountId | slovenia |
      | body.data   | test data  |
      | body.position.lat | 46.0569  |
      | body.position.lon | 14.5057  |
    Then I should receive event from room to which I have subscribed with status code: "200"
    When I subscribe to "spain" channel for route "test-system":"events" and trigger event with args:
      | body.accountId | spain |
      | body.data   | test data  |
      | body.position.lat | 40.2085  |
      | body.position.lon | -3.7310  |
    Then I shouldn't receive any event but response with null notification

