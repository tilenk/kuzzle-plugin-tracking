const
    should = require('should'),
    _ = require('lodash'),
    {
        When,
        Then
    } = require('cucumber');
 
var dot = require('dot-object');
 
const timer = ms => new Promise(res => setTimeout(res, ms))
 
const unflatten = (data) => {
    let receivedProperties = {}
    data.map((data) => {
        let property = dot.object({
            [data[0]]: data[1]
        })
        receivedProperties = _.merge(receivedProperties, property)
    })
    return receivedProperties
}
 
When('tests are initialized test indexes {string} with collections {string} and {string} are created', async function (index, collectionAccounts, collectionEvents) {
	this.response = {
        	exists: {
			index: await this.kuzzle.index.exists(index),
			[collectionAccounts]: await this.kuzzle.collection.exists(index, collectionAccounts),
			[collectionEvents]: await this.kuzzle.collection.exists(index, collectionEvents),
		}
    	}
});

Then('I should receive exists object with index, accounts and events properties set to {string}', function (expectedIsActive) {
    should(this.response).not.be.undefined();
    should(this.response.exists).be.Object();
    should(this.response.exists.index).be.eql(expectedIsActive === 'true')
    should(this.response.exists.accounts).be.eql(expectedIsActive === 'true')
    should(this.response.exists.events).be.eql(expectedIsActive === 'true')
});

When('tests are initialized count all documents', async function () {
    const accounts = await this.kuzzle.document.count("test-system", "accounts");
    const events = await this.kuzzle.document.count("test-system", "events");
    this.response = {
        counts: {
		accounts,
		events	
	}
    }
});

Then('I should receive {string} documents in events collection and {string} documents in accounts collection', function (expectedEvents, expectedAccounts) {
    should(this.response).not.be.undefined();
    should(this.response.counts).be.Object();
    should(this.response.counts.events).be.eql(parseFloat(expectedEvents))
    should(this.response.counts.accounts).be.eql(parseFloat(expectedAccounts))
});

When('I call the plugin route {string}:{string} with args:', async function (controller, action, dataTable) {
    const data = unflatten(dataTable.rawTable)
    try {
        const result = await this.kuzzle.document.create(
            controller,
            action,
            data.body
        );
 
        this.response = {
            result
        }
 
    } catch (error) {
        this.response = {
            error
        }
    }
});
 
 
Then('I should receive additional property selector with content: {string} and property isActive with content: {string}', function (expectatedSelector, expectedIsActive) {
    should(this.response).not.be.undefined();
    should(this.response.result).be.Object();
    should(this.response.result._source).be.Object();
    should(this.response.result._source.selector).be.eql(expectatedSelector)
    should(this.response.result._source.isActive).be.eql(expectedIsActive === 'true')
});
 
Then('I should receive response with precondition error with http status code: {string}', function (expectatedResult) {
    should(this.response).not.be.undefined();
    should(this.response.error).be.Object();
    should(this.response.error.status).be.eql(parseFloat(expectatedResult))
});
 
When('I subscribe to {string} channel for route {string}:{string} and trigger event with args:', async function (accountId, controller, action, dataTable) {
    const callback = async (notification) => {
        if (notification.scope === 'in') {
            this.response = {
                notification
            }
 
        }
    }
 
    const data = unflatten(dataTable.rawTable)
    const options = { scope: 'in' };
    let roomId = await this.kuzzle.realtime.subscribe(controller, action, {
        equals: {
            selector: `${accountId}true`
        }
    }, callback, options);
 
    const result = await this.kuzzle.document.create(
        controller,
        action,
        data.body
    );
})
 
Then('I should receive event from room to which I have subscribed with status code: {string}', function (expectatedResult) {
    should(this.response).not.be.undefined();
    should(this.response.notification).be.Object();
    should(this.response.notification.status).be.eql(parseFloat(expectatedResult))
    this.response = {
        notification: null
    }
});

Then('I shouldn\'t receive any event but response with null notification', function () {
    should(this.response).not.be.undefined();
    should(this.response.notification).be.eql(null);
});

