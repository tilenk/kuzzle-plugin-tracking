const eventMapping = {
    properties: {
        accountId: { 'type': 'keyword' },
        selector: { 'type': 'keyword' },
        isActive: { type: 'boolean' },
        data: { type: 'text'},
        position: { 'type': 'geo_point' }
    }
};
 
const accountMapping = {
    properties: {
        accountId: { 'type': 'keyword' },
        name: { 'type': 'keyword' },
        isActive: { type: 'boolean' },
        gpxTrigger: {
            properties: {
             delay: { type: 'integer' }
            }       
        },
    }
};
 
async function insertAccounts(kuzzle, accounts) {
    const promises = accounts.map(async (account) => {
        await kuzzle.document.create(
            'test-system',
            'accounts', {
                accountId: account.accountId,
                name: account.name,
                isActive: account.isActive,
                gpxTrigger: account.gpxTrigger
            },
            account.accountId,
            {
                queuable: true,
                refresh: 'wait_for'
            }
        );
    })
    return await Promise.all(promises)
}
 
module.exports.prepareSystemCollections = async(kuzzle) => {
    const accounts = [
        {
            name: 'Spain',
            accountId: 'spain',
            isActive: false,
            gpxTrigger: {
                delay: 0
           },
        },
        {
            name: 'Slovenia',
            accountId: 'slovenia',
            isActive: true,
            gpxTrigger: {
                delay: 1000
           },
        }
    ];
 
    if (await kuzzle.index.exists('test-system')) {
 	await kuzzle.index.delete('test-system');
    }

    await kuzzle.index.create('test-system');
    await kuzzle.collection.create('test-system', 'events', eventMapping);
    await kuzzle.collection.create('test-system', 'accounts', accountMapping);
    await insertAccounts(kuzzle, accounts)
}
 


