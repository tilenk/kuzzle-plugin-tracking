'use strict';
require('dotenv').config()
const { prepareSystemCollections } = require('./insertTestData.js')

const
  { After, Before } = require('cucumber'),
  { Kuzzle, WebSocket } = require('kuzzle-sdk')
 
Before(async function () {
  this.kuzzle = new Kuzzle(
    new WebSocket(process.env.KUZZLE_BACKEND_DOMAIN_NAME, { port: process.env.KUZZLE_BACKEND_PORT })
  );
 
  await this.kuzzle.connect();
  await this.kuzzle.auth.login('local', { username: process.env.KUZZLE_BACKEND_ADMIN_USERNAME, password: process.env.KUZZLE_BACKEND_ADMIN_PASSWORD });
 
/*
  await this.kuzzle.query({
    controller: 'admin',
    action: 'resetDatabase',
    refresh: 'wait_for'
  });
 */
  await prepareSystemCollections(this.kuzzle)
});
 
After(async function () {
  // Clean values stored by the scenario
  this.props = {};
 
  if (this.kuzzle && typeof this.kuzzle.disconnect === 'function') {
    await this.kuzzle.index.delete('test-system');
    this.kuzzle.disconnect();
  }
});
 
 
 


